/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;

import java.util.Scanner;

/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
public class CardTrick {
    
   public static void main(String[] args)
    {
        Scanner kb = new Scanner(System.in);
        Card[] magicHand = new Card[7];
        
        for (int i=0; i<magicHand.length; i++)
        {
            Card c = new Card();
            
            c.setValue(c.randomValue());                   
            c.setSuit(Card.SUITS[c.randomSuit()]);
                   
            magicHand[i] = c;
                       
        }
        
        //insert code to ask the user for Card value and suit, create their card
        System.out.println("Pick a card, any card");
        System.out.println("First pick the cards value: ");        
        int userValue = kb.nextInt();
        
        System.out.println("Now, pick the cards suit (Hearts, Diamonds, Spades, Clubs: ");   
        String userSuit = kb.next();
             
        System.out.println("You chose the " + userValue + " of " + userSuit);
        
        // and search magicHand here
        System.out.println("Let's check the magic hand...");
        for(Card magicHand1 : magicHand)
        {
            System.out.println(magicHand1.getValue() + " of " + magicHand1.getSuit());
            
            if(userValue == magicHand1.getValue())
            {
                if(userSuit.equals(magicHand1.getSuit()))
                {
                    System.out.println("Congrats! You won!");
                    break;
                }
               
            } 
            else
            {
               System.out.println("Not your card, let's try again!"); 
            }
        }
       
        
    }
    
}
